from pydantic import BaseModel
import json

class ScraperSetting(BaseModel):
    action_interval: int
    max_retry: int
    retry_interval: int
    upvote_interval_hours: int

class Logger(BaseModel):
    level: str
    file_path: str

class ConfigProvider(BaseModel):
    logging: Logger
    scraper_setting: ScraperSetting
    karuta_bot_url: str
    
    @classmethod
    def get_config(cls, json_file: str = "config.json") -> 'ConfigProvider':
        with open(json_file, 'r') as file:
            data = json.load(file)
        return cls(**data)
