import logging
import logging.config

def singleton(cls):
    instances = {}
    
    def get_instance(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    
    return get_instance

@singleton
class LogProvider:
    def __init__(self, file_path: str, level: str):
        self.file_path = file_path
        self.level = level
        self._setup_logger()
        
    def _parse_logging_level(self, level: str) -> int:
        match (level):
            case "INFO":
                return logging.INFO
            case "DEBUG":
                return logging.DEBUG
            case "ERROR":
                return logging.ERROR
    
    def _setup_logger(self):
        self.logger = logging.getLogger("Karuta Logger")
        logging_level = self._parse_logging_level(self.level)
        self.logger.setLevel(logging_level)
        
        file_handler = logging.FileHandler(self.file_path)
        file_handler.setLevel(logging_level)
        
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        
        if not self.logger.hasHandlers():
            self.logger.addHandler(file_handler)
    
    def log_info(self, message: str):
        self.logger.info(message)
    
    def log_debug(self, message: str):
        self.logger.debug(message)
    
    def log_error(self, message: str):
        self.logger.error(message)
    
         