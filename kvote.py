import pyautogui
import time
from log_provider import LogProvider
from config_provider import ConfigProvider
import textwrap

class KarutaAutoVote:
    def __init__(self):
        self.config = ConfigProvider.get_config()
        
        log_path = self.config.logging.file_path
        log_level = self.config.logging.level
        self.__logger = LogProvider(log_path, log_level)
        pyautogui.PAUSE = self.config.scraper_setting.action_interval
        
    def open_karuta(self):
        pyautogui.hotkey('win', 'r')
        pyautogui.write('chrome')
        pyautogui.press('enter')
        pyautogui.write(self.config.karuta_bot_url)
        pyautogui.press('enter')
        self.__logger.log_info("Attemping to navigate to Karuta - top.gg")
    
    def find_and_click_vote(self):
        retry_limit = self.config.scraper_setting.max_retry
        while retry_limit >= 0:
            retry_limit -= 1
            try:
                button_location = pyautogui.locateOnScreen("asset/vote_button.png", confidence=0.5)
                button_center = pyautogui.center(button_location)
                pyautogui.moveTo(button_center)
                pyautogui.click(button_center)
                self.__logger.log_info("Found upvote button - Successfully upvoted")
                break
            except pyautogui.ImageNotFoundException as e:
                retry_interval = self.config.scraper_setting.retry_interval
                self.__logger.log_error(f"Unable to find upvote button. {retry_limit} retry remaining."
                                        f"Programme pausing for {retry_interval} seconds")
                time.sleep(retry_interval)
    
    # TODO
    # If we see the login page (With just login button), just login
    # In the future can extend to input username & password 
    def login():
        pass
    
    # TODO
    # Run the auto vote process every inverval hours according to config
    def auto_vote():
        pass
    
    
            
voter = KarutaAutoVote()
voter.open_karuta()
voter.find_and_click_vote()